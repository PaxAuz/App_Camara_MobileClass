package luisauz.facci.a4acamara;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;

public class MainActivity extends AppCompatActivity
implements View.OnClickListener{

    Button buttonCamera, buttonSave;
    ImageView imageViewPhoto;
    Bitmap imageBitmap;

    static final int REQUEST_IMAGE_CAPTURE = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonCamera = (Button) findViewById(R.id.buttonCamera);
        buttonSave = (Button) findViewById(R.id.buttonSave);
        imageViewPhoto = (ImageView) findViewById(R.id.imageViewPhoto);
        buttonCamera.setOnClickListener(this);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private static final int PERMISSION_SAVE = 101;

    public void saveImage(){
        if(passVerification()){
            createFolder();
        }
        else{
            ActivityCompat.requestPermissions
                    (this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            PERMISSION_SAVE);
        }
    }

    private void createFolder() {
        String nameDirectory = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "Images4A";
        File folder = new File(nameDirectory);
        if (!folder.exists()) {
            if (folder.mkdir()) {
                Toast.makeText(this, "Created Folder", Toast.LENGTH_LONG).show();
            }
        }
    }
    private boolean passVerification(){
        return ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onClick(View view){
        Intent takePhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePhoto.resolveActivity(getPackageManager()) != null){
            startActivityForResult(takePhoto, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK){
            Bundle extras = data.getExtras();
            imageBitmap = (Bitmap) extras.get("data");
            imageViewPhoto.setImageBitmap(imageBitmap);
        }
    }
}
